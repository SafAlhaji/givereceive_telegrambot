import logging
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters
)
from api import (get_cities, get_tags, create_give)
from telegram import ReplyKeyboardRemove 
# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

TAG, REGION, DESCRIPTION, NUMBER, END = range(5)

OREGION, OEND=range(2)

def find_items(item,region):
    return []

async def start(update: Update, context: ContextTypes):
    keyboard = [['تبرع'], ['طلب']]
    await update.message.reply_text(
        "مرحباً بك! يمكنك من خلال هذا البوت التبرع او الحصول على المساعدة. "
        "أرسل /cancel لإيقاف العملية.\n\n"
        "هل تود بالتبرع أو الحصول على مساعدة؟",
        reply_markup=ReplyKeyboardMarkup(
            keyboard, one_time_keyboard=True, input_field_placeholder="تبرع أو طلب؟"
        ),
    )

async def item(update: Update, context: ContextTypes) -> int:
    await update.message.reply_text("بماذا تريد ان تتبرع؟")
    return TAG

async def tag(update: Update, context: ContextTypes) -> int:
    title = update.message.text
    context.user_data['title'] = title
    keyboard = list(map(lambda x: [x], get_tags()))
    await update.message.reply_text("ماذا تخص المادة؟", reply_markup=ReplyKeyboardMarkup(keyboard))
    return REGION

async def reg(update: Update, context: ContextTypes) -> int:
    item = update.message.text
    context.user_data['tags'] = [item]
    keyboard = list(map(lambda x: [x], get_cities()))
    await update.message.reply_text("ادخل المنطقة المراد التبرع لها", reply_markup=ReplyKeyboardMarkup(keyboard))
    return DESCRIPTION

async def description(update: Update, context: ContextTypes) -> int:
    region = update.message.text
    context.user_data['city'] = region
    markup=ReplyKeyboardRemove()
    await update.message.reply_text("ادخل شرح بسيط عن التبرع", reply_markup=markup)
    return NUMBER

async def num(update: Update, context: ContextTypes) -> int:
    desc = update.message.text
    context.user_data['details'] = desc
    await update.message.reply_text("ادخل رقم الهاتف الخاص بك",reply_markup=ReplyKeyboardRemove())
    return END

async def end(update: Update, context: ContextTypes) -> int:
    phone_number=update.message.text
    context.user_data['phoneNumber']=phone_number
    data = context.user_data
    if create_give(data):
        await update.message.reply_text("تم التبرع بنجاح!",reply_markup=ReplyKeyboardRemove())
    else :
        await update.message.reply_text("حدث خطأ ما الرجاء المحاولة مرة أخرى")
        await update.message.reply_text("ارسل كلمة (تبرع) بدون اقواس للمحاولة مرة أخرى")
    keyboard = [['تبرع'], ['طلب']]
    await update.message.reply_text("ماذا تريد ان تفعل؟",reply_markup=ReplyKeyboardMarkup(keyboard))
    context.user_data.clear()
    return ConversationHandler.END

async def cancel(update: Update, context: ContextTypes) -> int:
    await update.message.reply_text("تم الالغاء")
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    keyboard = [['تبرع'], ['طلب']]
    await update.message.reply_text("ماذا تريد ان تفعل؟",reply_markup=ReplyKeyboardMarkup(keyboard))
    context.user_data.clear()
    return ConversationHandler.END

async def order(update: Update, context: ContextTypes) -> int:
    keyboard = [['حليب'], ['بطانيات'], ['ملابس'],['فحص زلازل'],['غذاء'],['حفاضات اطفال'],['فوط نسائية'],['متطوعين'],['نقل']]
    await update.message.reply_text("ماذا تحتاج؟",reply_markup=ReplyKeyboardMarkup(keyboard,one_time_keyboard=True))
    return OREGION

async def oreg(update: Update, context: ContextTypes) -> int:
    item=update.message.text
    context.user_data['item']=item
    keyboard = [['جبلة'],[ 'اللاذقية'], ['دمشق'], ['حلب'],[ 'حمص']]
    await update.message.reply_text("ادخل منطقة تواجدك", reply_markup=ReplyKeyboardMarkup(keyboard))
    return OEND

async def oend(update: Update, context: ContextTypes) -> int:
    oreg=update.message.text
    item=context.user_data['item']
    d={"d":"d"}
    
    data=find_items(item,oreg)

    ready_reponse=f"يمكنك طلب {item} من الارقام التالية في {oreg}\n\n"
    cnt=0
    for i in data:
        cnt+=1
        ready_reponse+=f" {i.get('phone_number')}\n\n "
    if cnt==0:
        await update.message.reply_text(f'لا يوجد في {item} متوفر في منطقتك حاليا',reply_markup=ReplyKeyboardRemove())
    else :
        
        print (ready_reponse)
        await update.message.reply_text(ready_reponse,reply_markup=ReplyKeyboardRemove())
    
    keyboard = [['تبرع'], ['طلب']]
    await update.message.reply_text("ماذا تريد ان تفعل؟",reply_markup=ReplyKeyboardMarkup(keyboard))
    return ConversationHandler.END

def main():
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(
        "6041068333:AAGdF1ZWsPAtLVVVu0Ahr-mWo37ZAPwjdPU").build()
    donate_handler = ConversationHandler(
        entry_points=[MessageHandler(filters.Regex("تبرع") , item)  , CommandHandler("donate",item)],
        states={
            # OBJECT: [MessageHandler(filters.Regex("^ملابس|بطانيات|حليب"), item)],
            TAG: [MessageHandler(filters.TEXT, tag)],
            REGION: [MessageHandler(filters.TEXT, reg)],
            DESCRIPTION: [MessageHandler(filters.Regex("^جبلة|اللاذقية|حمص|حلب|دمشق"), description)],
            NUMBER: [MessageHandler(filters.TEXT, num)],
            END: [MessageHandler(filters.TEXT, end)]
        },
        fallbacks=[MessageHandler(filters.Regex("الغاء"), cancel),CommandHandler("cancel", cancel)],
    )

    application.add_handler(donate_handler)
    application.add_handler(CommandHandler("start", start))

    order_handler = ConversationHandler(
        entry_points=[MessageHandler(filters.Regex("طلب") , order)  , CommandHandler("order",order)],
        states={
            OREGION: [MessageHandler(filters.TEXT, oreg)],
            OEND: [MessageHandler(filters.TEXT, oend)]
        },
        fallbacks=[CommandHandler("cancel", cancel),MessageHandler(filters.Regex("الغاء"), cancel)],
    )
    application.add_handler(order_handler)
    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()
