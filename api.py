import requests
import json

base_url = "https://syrgiveapi-test.onrender.com"

def get_tags():
    response = requests.get(base_url + '/tags/getall')
    body = response.json()
    return list(map(lambda x: x['tag'], body))

def get_cities():
    response = requests.get(base_url + '/city/getall')
    body = response.json()
    return list(map(lambda x: x['city_name'], body))

def create_give(data):
    print(data)
    response = requests.post(base_url + '/give/', json=data)
    print(str(response.content))
    return str(response.status_code)[0] == '2'

# Test:
# create_give({'title': 'test', 'tags': ['حرامات'], 'city': 'حلب', 'details': 'test test', 'phoneNumber': '09123123123'})